import pytest
import random
from chainmiddleware.cache import StateCache, CachedNodeMiddleware
from chainmiddleware.node import BlockchainNode
from chainmiddleware.middleware import state_transfer


@pytest.fixture
def cache_size():
    return 3


@pytest.fixture
def cached_node(blockchain_node, cache_size):
    return CachedNodeMiddleware(blockchain_node, cache_size)


@pytest.fixture
def state_cache(cached_node, cache_size):
    return StateCache(cached_node, cache_size, state_transfer)


def make_random_hash():
    return hash(random.random())


def make_block(number, prevhash, is_genesis_block=False):

    block = {
        "number": number,
        "hash": make_random_hash(),
        "prevhash": prevhash,
        "transfers": [{"sender": "0xb33f", "receiver": "0x992f", "amount": 1}],
    }
    if is_genesis_block is True:
        block["balances"] = {"0xb33f": 10000, "0x992f": 0}
    return block


def test_cache(state_cache, cached_node):
    sink_address = "0x992f"

    genesis = make_block(0, make_random_hash(), is_genesis_block=True)
    a1 = make_block(1, genesis["hash"])
    b1 = make_block(1, genesis["hash"])
    # only in this fork, one block doesn't have a transfer
    b1["transfers"] = []
    b2 = make_block(2, b1["hash"])
    b3 = make_block(3, b2["hash"])

    b4 = make_block(4, b3["hash"])
    b5 = make_block(5, b4["hash"])
    blocks = [genesis, a1, b1, b2, b3, b4, b5]

    # HACK inject the new block data
    # TODO do this in the fixtures
    cached_node.node = BlockchainNode(blocks, address="0xb33f")

    genesis_block = cached_node.get_current_block()
    state_cache.apply_genesis_block(genesis_block["hash"])
    assert state_cache.left_window_state == genesis_block["balances"]

    assert state_cache.current_state_block_hash == genesis_block["hash"]
    assert state_cache.current_state[sink_address] == 1
    # the left window shouldn't have changed and should be the same
    # as the initial balances
    assert state_cache.left_window_state[sink_address] == 0

    a1_retrieved = cached_node.get_current_block()

    state_cache.apply_block(a1_retrieved["hash"])
    assert state_cache.current_state_block_hash == a1_retrieved["hash"]
    assert state_cache.left_window_block_hash == genesis_block["prevhash"]
    assert state_cache.current_state[sink_address] == 2
    assert state_cache.left_window_state[sink_address] == 0

    b1_retrieved = cached_node.get_current_block()
    b2_retrieved = cached_node.get_current_block()

    fork_path = [block["hash"] for block in [b1_retrieved, b2_retrieved]]
    # now do a reorg manually:
    state_cache.replay_from_cache(genesis["hash"], fork_path)

    assert state_cache.current_state_block_hash == b2_retrieved["hash"]
    # although we have now 3 blocks, the balance is only 2,
    # since one block didn't have the transaction

    assert state_cache.current_state_block_hash == b2_retrieved["hash"]
    assert state_cache.left_window_block_hash == genesis_block["prevhash"]
    assert state_cache.current_state[sink_address] == 2
    assert state_cache.left_window_state[sink_address] == 0

    # now traverse the cache above the window size

    b3_retrieved = cached_node.get_current_block()
    state_cache.apply_block(b3_retrieved["hash"])
    assert state_cache.current_state_block_hash == b3_retrieved["hash"]
    assert state_cache.left_window_block_hash == genesis_block["hash"]
    assert state_cache.current_state[sink_address] == 3
    # now we applied the transfers from the genesis state
    assert state_cache.left_window_state[sink_address] == 1

    b4_retrieved = cached_node.get_current_block()
    state_cache.apply_block(b4_retrieved["hash"])
    assert state_cache.current_state_block_hash == b4_retrieved["hash"]
    assert state_cache.left_window_block_hash == b1_retrieved["hash"]
    assert state_cache.current_state[sink_address] == 4
    # now b1 got applied to the left cache, not changing the balance, since this block
    # doesn't have transactions
    assert state_cache.left_window_state[sink_address] == 1

    b5_retrieved = cached_node.get_current_block()
    state_cache.apply_block(b5_retrieved["hash"])
    assert state_cache.current_state[sink_address] == 5
    # now b3 got applied to the left cache
    assert state_cache.left_window_state[sink_address] == 2
    # now apply the whole chain again from the genesis block
    # and check if the result is still the same
    current_chain = [block["hash"] for block in blocks if block is not a1]
    state_cache.rebuild_from_genesis(current_chain)

    assert state_cache.current_state[sink_address] == 5
    assert state_cache.left_window_state[sink_address] == 2
