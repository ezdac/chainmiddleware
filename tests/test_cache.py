import pytest

from chainmiddleware.cache import BlockHeightCache


@pytest.fixture
def max_window_size():
    return 3


@pytest.fixture
def cache(max_window_size):
    return BlockHeightCache(max_window_size)


def test_block_height_cache_cleanup(max_window_size, cache, blockchain_node):
    genesis_block = blockchain_node.get_current_block()
    other_block = blockchain_node.get_current_block()
    invalidate_block = blockchain_node.get_current_block()
    far_out_block = blockchain_node.get_current_block()

    # use the node to get some blocks, but enforce the block numbers to
    # be how we want them
    genesis_block["number"] = 0
    other_block["number"] = 1
    invalidate_block["number"] = 3
    far_out_block["number"] = 999

    cache.add_block(genesis_block)
    cache.add_block(other_block)
    assert cache.current_block_heigth == 1
    assert len(cache.blocks) == 2
    cache.cleanup()

    # cache should have memorized all blocks, since they are within the cache window
    # size
    assert cache.current_block_heigth == 1
    assert len(cache.blocks) == 2

    cache.add_block(invalidate_block)
    cache.cleanup()

    # the genesis block should have been invalidated now, the
    # size of the cache stays the same
    assert cache.current_block_heigth == 3
    assert len(cache.blocks) == 2

    cache.add_block(far_out_block)
    cache.cleanup()

    # only the far out bock should be in the cache now
    assert len(cache.blocks) == 1
    assert cache.current_block_heigth == 999


def test_add_block(blockchain_node, cache):
    genesis_block = blockchain_node.get_current_block()
    other_block = blockchain_node.get_current_block()
    assert cache.current_block_heigth == 0
    assert len(cache.block_hash_map) == 0
    assert len(cache.blocks) == 0
    cache.add_block(genesis_block)

    assert cache.get(genesis_block["hash"]) == genesis_block
    assert cache.current_block_heigth == genesis_block["number"]
    assert len(cache.block_hash_map) == 1
    assert len(cache.blocks) == 1

    cache.add_block(other_block)

    assert cache.get(other_block["hash"]) == other_block
    assert cache.current_block_heigth == other_block["number"]
    assert len(cache.block_hash_map) == 2
    assert len(cache.blocks) == 2

