import pytest


from chainmiddleware.middleware import Middleware
from chainmiddleware.cache import CachedNodeMiddleware


@pytest.fixture
def cache_window_size():
    return 6


@pytest.fixture
def client():
    # TODO
    return None


@pytest.fixture
def middleware(client, blockchain_node, cache_window_size):
    return Middleware(client, blockchain_node, cache_window_size)


def test_middleware(middleware, blockchain_node):
    # iterator, will iterate until StopIteration is raised
    try:
        while True:
            current_block = middleware.node.get_current_block()
            middleware.process_new_block(current_block)
    except StopIteration:
        pass

    assert all([value >= 0 for value in middleware.balances.values()])

    chain_numbers = [
        middleware.node.get_block(block_hash)["number"]
        for block_hash in middleware.current_chain
    ]
    assert chain_numbers == list(range(chain_numbers[-1] + 1))

    # TODO write some more informed test assertions and check for correct reorgs

