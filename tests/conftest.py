import pytest
import os
from pathlib import PosixPath

from chainmiddleware.node import BlockchainNode, get_block_data


DIRPATHK = PosixPath(os.path.dirname(os.path.abspath(__file__)))


@pytest.fixture
def address():
    return "0x132118"


@pytest.fixture
def blocks():
    return get_block_data(str(DIRPATHK / "blocks.json"))


@pytest.fixture
def blockchain_node(address, blocks):
    return BlockchainNode(blocks, address)

