import json
import random


class BlockchainNode:
    def __init__(self, blocks, address=None):
        self.address = address or random.choice(list(blocks[0]["balances"].keys()))
        self.blocks = {block["hash"]: block for block in blocks}
        self._block_iterator = iter(blocks)

    def get_current_block(self):
        return next(self._block_iterator)

    def get_block(self, block_hash):
        return self.blocks.get(block_hash)


def get_block_data(json_file_path):
    with open(json_file_path, "r") as f:
        return json.load(f)

