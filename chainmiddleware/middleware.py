import math
import random
from collections import defaultdict

import networkx as nx

from chainmiddleware.node import get_block_data
from chainmiddleware.cache import StateCache, CachedNodeMiddleware


def state_transfer(block, state_dict):
    for tx in block["transfers"]:
        sender = tx["sender"]
        receiver = tx["receiver"]
        amount = tx["amount"]
        try:
            if state_dict[sender] - amount < 0:
                # raise Exception(f"Insufficient funds for {sender}")

                # FIXME, I think the block data generation in mockchain.py
                # doesn't account for this
                pass
            state_dict[sender] -= amount
        except KeyError:
            raise KeyError(f"Sender {sender} has no account!")
        try:
            state_dict[receiver] += amount
        except KeyError:
            state_dict[receiver] = amount


class Middleware:
    def __init__(self, client, node, cache_window_size):
        self.node = CachedNodeMiddleware(node, cache_window_size)
        self.state_cache = StateCache(self.node, cache_window_size, state_transfer)
        self.dag = nx.DiGraph()
        self.current_chain = []
        self.blocks = {}
        self.genesis_hash = None

    @property
    def balances(self):
        return self.state_cache.current_state

    def process_new_block(self, block):
        parent_hash = block["prevhash"]
        block_hash = block["hash"]
        self.dag.add_node(block_hash, height=block["number"])
        try:
            # check if this is the genesis block
            self.dag[parent_hash]
            self.dag.add_edge(parent_hash, block_hash)
        except KeyError:
            assert block["number"] == 0
            # set the initial balances if we first receive the genesis block
            self.genesis_hash = block["hash"]
            self.state_cache.apply_genesis_block(block_hash)

        # Find the consensus chain by running a longest path find on the
        # global view on the blockchain. This could probably be implemented
        # more efficiently, with added memoization, since we currently run over
        # the whole graph for every new block.
        # In Ethereum, the modified GHOST algorithm determines on the 'heaviest' fork,
        # which also includes uncles and transactions etc.

        # FIXME which path to chose if multiple paths are equivalent?
        # We should probably do a reorg ONLY if there is a NEW fork that
        # is longer then the current consensus fork.
        # If we just choose one of them arbitrarily (as is implemented in the networkx
        # algorithm), then this could result in frequent reorgs.
        # E.g. if we have 2 equivalently long forks, where we get the blocks
        # of the forks always alternating from the 2 forks, then it is possible
        # that we do a reorg every time!
        longest_path = nx.algorithms.dag.dag_longest_path(self.dag)
        if path_is_subpath(longest_path, self.current_chain):
            # we appended to the current path and don't need to do anything
            self.state_cache.apply_block(block["hash"])
            print("Appended to consensus fork")
        else:
            fork_point = paths_get_fork_point(self.current_chain, longest_path)
            if fork_point:
                fork_index = longest_path.index(fork_point)
                fork_path = longest_path[fork_index + 1 :]
            else:
                raise Exception(
                    "No fork point found, not even the genesis block."
                    "Malformed block data."
                )

            try:
                self.state_cache.replay_from_cache(fork_point, fork_path)
            except KeyError:
                # the fork point is not in the state cache window,
                # so no matter what we have to rebuild it from the
                # genesis block on
                self.state_cache.rebuild_from_genesis(longest_path)
            print("Reorg detected!")
        self.current_chain = longest_path

    def get_balance_median(self):
        # if we query more often then insert items into the balance cache,
        # this would be inefficient,
        # especially with big number of accounts, or if the balance cache doesn't
        # fit in memory and disk IO has to be done
        balances = sorted(self.balances.values())
        return balances[math.ceil(len(balances) / 2)]


def generate_tx_hash(transfer, block_hash):
    return hash(
        map(
            hash,
            [transfer["amount"], transfer["receiver"], transfer["sender"], block_hash],
        )
    )


def paths_get_fork_point(path, subpath):
    """
    INEFFICIENT implementation of finding the 
    branching points of 2 aligned Directed Paths
    Makes some assumptions about directedness and sort order
    """

    common_elements = subpath_common_elements(path, subpath)
    if common_elements:
        fork_point_candidate = common_elements[-1]
        if path.index(fork_point_candidate) == len(path) - 1:
            # paths overlap in the end
            return None
        else:
            return fork_point_candidate
    else:
        # there is no overlap, so no fork point
        return None


def subpath_common_elements(path, subpath):
    return [elem for elem in subpath if elem in set(path)]


def path_is_subpath(path, subpath):
    common_elements = subpath_common_elements(path, subpath)
    return len(common_elements) == len(subpath)

