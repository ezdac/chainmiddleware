from collections import defaultdict
from copy import copy
from collections import deque


class BlockHeightCache:
    """
    Keeps blocks in cache,
    as long as they are not older then max_window_size.
    This is handy for when a lot of reorgs happen close 
    to the current HEAD, since we don't have to
    query for blocks from the node.
    """
    def __init__(self, max_window_size):
        self.max_window_size = max_window_size
        self.current_block_heigth = 0
        self.blocks = defaultdict(list)
        self.block_hash_map = {}

    def add_block(self, block):
        height = block["number"]
        self.current_block_heigth = max(self.current_block_heigth, height)
        self.blocks[height].append(block)
        # store in order to make a hash based lookup quicker
        self.block_hash_map[block["hash"]] = height

    def cleanup(self):
        for height, blocks in list(self.blocks.items()):
            if self.current_block_heigth - height < 0:
                continue
            if self.current_block_heigth - height >= self.max_window_size:
                # prune this and remove from the cache
                for block in blocks:
                    del self.block_hash_map[block["hash"]]
                del self.blocks[height]

    def get(self, block_hash):
        try:
            height = self.block_hash_map[block_hash]
            for block in self.blocks[height]:
                if block["hash"] == block_hash:
                    return block
                return None
        except KeyError:
            return None


class StateCache:
    def __init__(self, node, max_size, state_transfer_func):
        """
        A sliding window cache, that keeps a queue of blocks 
        that represent the head of length `max_size` of the current
        consensus fork.

        This class keeps 2 state caches, that represent checkpoints 
        of the blockchain state.
        The left_window_state is a checkpoint reaching in the past, 
        in order to do fast reorgs within the cache window.
        The current_state is the most recent state, in order 
        do achieve fast state queries by the client.
        
        The StateCache receives block hashes to apply to the state window and 
        asks a blockchain node for the actual block data to apply 
        Ideally, this node should be a wrapped node with a cached view of the head 
        of the whole block-graph (also 'stale' forks), which is the same size as 
        the state window. That way, reorgs that happen within the state window 
        are efficient and don't need to query a lot of blocks from the (remote) node.
        """
        self.max_size = max_size
        self.hash_queue = deque(maxlen=self.max_size)
        self.left_window_state = {}
        self.left_window_block_hash = None
        self.current_state = {}
        self.current_state_block_hash = None
        self._state_transfer_func = state_transfer_func

        self.node = node
        self.consensus_transactions = {}

    @classmethod
    def wrap_from_node(cls, node, max_size, state_transfer_func):
        """
        Takes a remote node, and wraps it with a cache middleware with the same
        window size as the StateCache.
        """
        return cls(CachedNodeMiddleware(node, max_size), max_size, state_transfer_func)

    @property
    def cached_block_hashes(self):
        return set(self.hash_queue)

    def _retrieve_block(self, block_hash):
        assert isinstance(block_hash, int)
        return self.node.get_block(block_hash)

    def _popleft(self):
        block_hash = self.hash_queue.popleft()
        return block_hash, self._retrieve_block(block_hash)

    def apply_genesis_block(self, block_hash):
        block = self._retrieve_block(block_hash)
        block_hash = block["hash"]
        self.left_window_state = copy(block["balances"])
        # the left cache represents the state BEFORE applying the transactions
        # in the genesis block
        self.left_window_block_hash = block["prevhash"]

        self.current_state = copy(block["balances"])
        self.current_state_block_hash = block_hash

        self.apply_block(block_hash)

    def apply_block(self, block_hash):
        # simply adds a block to the queue wihtout asking questions,
        # applying the txs of the next block in the queue to the left window state

        block = self._retrieve_block(block_hash)
        # first apply the block to the current state
        self.apply_state(block, self.current_state)
        self.current_state_block_hash = block_hash

        # now eventually traverse the cache window on step further by inserting the
        # block in the queues
        if len(self.hash_queue) >= self.hash_queue.maxlen:
            self.left_window_block_hash, next_cached_block = self._popleft()
            self.apply_state(next_cached_block, self.left_window_state)

        self.hash_queue.append(block["hash"])

    def rebuild_from_genesis(self, forked_block_hashes):
        #This assumes the first block in the chain is the genesis block
        # with an inital balances extra field

        # reinitialize the object, since we rebuild anyways
        self.__init__(self.node, self.max_size, self._state_transfer_func)

        forked_block_hashes = iter(forked_block_hashes)
        self.apply_genesis_block(next(forked_block_hashes))

        # apply all blocks in the chain and let the cache window traverse
        for block in forked_block_hashes:
            self.apply_block(block)

    def replay_from_cache(self, fork_point_hash, forked_block_hashes):
        """
        Replays the current chain from the current left window
        cache, if possible.
        `forked_block_hashes` is the new chain to be used,
        but only from the branching point of the current chain on.
        If this is a new chain, that doesn't branch off from the current chain,
        this will raise a KeyError, and the State has to be rebuilt from the 
        genesis block on
        """
        available_state_hashes = set(self.hash_queue)
        available_state_hashes.add(self.left_window_block_hash)
        if fork_point_hash not in available_state_hashes:
            raise KeyError("Parent hash not found in cache")

        # set current state to left window state,
        # which is the state BEFORE the left_window_block_hash
        self.current_state = copy(self.left_window_state)
        self.current_state_block_hash = self.left_window_block_hash

        idx = self.hash_queue.index(fork_point_hash)
        # remove the part of the chain that will be invalidated
        for _ in range(len(self.hash_queue) - idx - 1):
            self.hash_queue.pop()

        # now apply the state from the left cache until the fork point
        for block_hash in self.hash_queue:
            block = self._retrieve_block(block_hash)
            self.apply_state(block, self.current_state)
            self.current_state_block_hash = block["hash"]

        # add and apply the new blocks from the fork,
        # this can also traverse the window etc
        for block_hash in forked_block_hashes:
            self.apply_block(block_hash)

    def apply_state(self, block, state_dict):
        self._state_transfer_func(block, state_dict)


class CachedNodeMiddleware:
    """
    Sits between the Node and the Middleware.
    Caches new blocks with a certain block height,
    transparently delegates requests for specific blocks
    either to its internal cache (if still cached) or
    queries the block from the node.
    """ 
    def __init__(self, node, cache_window_size):
        self.cache = BlockHeightCache(cache_window_size)
        self.node = node

    def get_current_block(self):
        block = self.node.get_current_block()
        self.cache.add_block(block)
        self.cache.cleanup()
        return block

    def get_block(self, block_hash):
        block = self.cache.get(block_hash)
        if not block:
            block = self.node.get_block(block_hash)
        return block

