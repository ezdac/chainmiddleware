# Implementation

Install with poetry and then run:
`poetry run pytest tests`

The most interesting test is the `tests/test_chainmiddleware.py`, since there the middleware is used with 
the generated chain-data from the `mockchain.py` script.

What is missing:
 - A mock for the Client that consumes the Middleware
 - An endpoint in the Middleware, where only certain balances can be queried. In this case this would just filter 
 the `middleware.balances` dictionary for the keys the client is interested in
 - Transfers where the client address is involded in are not memorized yet, and thus can't be queried.
   It was planned that the `StateCache` class builds a memory of those transactions while blocks are added
   by filtering the transactions, that are applied to the state, for the client address.
   When a reorg happens, the transaction memory will be invalidated (to the genesis state or a cached state) 
   and during the application of the reorg state-transfers will be automatically kept up to date with the currently
   accepted consensus fork.


# no In-Memory state cache
If something can't be held in memory it has to be persisted on disk, and thus
be kept in some kind of database.

The characteristics needed from this database depends on the application, but 
here it is assumed that the client will:
 - don't change the subset of balances it is interested in (often)
 - the Client app query frequency is the same (or lower) as the frequency of new blocks

This means that that the insert and query importance are about the same:
 - on every block update there will be the whole state-updates inserted
 - on every block update the client queries some balances, but initiates the
 	calculation of the median, which needs to query all balances

Critical characteristics:
 - fast insert time for state updates (balance)
 - fast query times
 - edit times less important since reorgs not that often
 - only one datatype
 - not relational

A good datastructure for this database would probably be a simple Hashtable or a Trie,
which have similar insert and retrieval characteristics.

For the median, a quickselect could be applied to the values of the hashmap.
This way, we get a runtime of O(n log n) without keeping the whole array
in memory.

# What has the client to concider?
If the client also keeps some state, or initiates some actions 
based on the median or the balances that have some 'write' qualities,
then there is no finality, and some inconsistend state in the client 
could emerge upon reorganizations.

The client can only display those values, since it doesn't know
anything about when some older state will be invalidated retroactively.

# Edge cases
In my implementation, I only keep one cache of states, which is lagging behind
the HEAD block some fixed number of blocks.
If this size is reasonably chosen, the probablity that a completely new 
consensus fork will emerge that forked off BEFORE the memorized state is very low.

If it would happen, we would have to apply the whole chain of state updates from the beginning
of the blockchain, so the genesis block.

Therefore, we first of all have to eventually query a large number of blocks 
from the node, and second have to do a lot of state updates, which can be 
computationally heavy (*e.g. for a smart contract based blockchain where all
contract computations have to be done)

This can be easily avoided by chosing the cache size reasonably 
(e.g. 6 in Ethereum, which is recommended for state finality),
considering the tradeoff between storage and reorg probablities.


# Client-Middleware communication

The middleware queries the blockchain node for new blocks.
Upon receiving a new block, it first processes the new block, and handles 
a reorg if necessary. As soon as the state-cache and the median is updated,
it notifies the client of the block change, which then can (if necessary) initiate
a poll for the desired information (balances, median), which is already cached in the middleware.
The notification can be either handled with long polling, or with push notifications.


